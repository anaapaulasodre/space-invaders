#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<unistd.h>
#include<string.h>
#include "funcoes_jogo.h"

/* obs a identacao foi feita a partir do editor visual studio code */
/* imprime o placar atual do jogo */
void print_placar (int score, int rodadas) {
	wattron(stdscr, COLOR_PAIR(4));
	move(1,34);
	printw("Score: %d | Rodada %d/3",score,rodadas+1);
}

void desenha_borda () {
	int i, j;

	wattron(stdscr, COLOR_PAIR(6));
	i = 0;
	j = 0;
	move(i,j);
	printw("+");

	for (j = 1; j < 100; j++) {
		move(i,j);
		printw("-");
	}

	move(i,j);
	printw("+");

	for (i = 1; i < 36; i++) {
		move(i,j);
		printw("|");
	}

	move(i,j);
	printw("+");

	j = 0;
	for (i = 1; i < 36; i++) {
		move(i,j);
		printw("|");
	}

	move(i,j);
	printw("+");

	for (j = 1; j < 100; j++) {
		move(i,j);
		printw("-");
	}


}

/* cria um alien conforme suas caracteristicas */
void cria_alien (t_lista *alien, int alien_x, int alien_y, int id_alien, int tipo_alien) {
	t_nodo *p;
	p = alien->ini;
	while (p->ID != id_alien) 
		p = p->prox;

	p->coord.x = alien_x;
	p->coord.y = alien_y;
	p->max.x = alien_x + 2;
	p->max.y = alien_y + 4;
	p->tam_x = 3;
	p->tam_y = 5;
	p->direcao = direita;
	p->status = vivo;
	p->tipo = tipo_alien;
}

/* funcao que organiza a lista de aliens, lidando com cada tipo de alien do jogo 
e suas posicoes */
void inicia_aliens (t_lista *ALIEN) {
	int alien_x = 6, alien_y = 4, id_alien = 0;
	while (id_alien < 55) {
        if (id_alien == 11 || id_alien == 22 || id_alien == 33 || id_alien == 44) {
            alien_y = 4;
            alien_x += 4;
        }
        if (id_alien < 11) {
            insere_fim_lista(id_alien,ALIEN);
            cria_alien(ALIEN,alien_x,alien_y,id_alien,tipo_alien1);
            alien_y += 7;
        }
        else if (id_alien < 22) {       
    	    insere_fim_lista(id_alien,ALIEN);
    	    cria_alien(ALIEN,alien_x,alien_y,id_alien,tipo_alien2);
            alien_y += 7;
        }
        else if (id_alien < 33) {
            insere_fim_lista(id_alien,ALIEN);
            cria_alien(ALIEN,alien_x,alien_y,id_alien,tipo_alien2);
            alien_y += 7;
        }
        else if (id_alien < 44) {
            insere_fim_lista(id_alien,ALIEN);
            cria_alien(ALIEN,alien_x,alien_y,id_alien,tipo_alien3);
            alien_y += 7;
        }
        else {
            insere_fim_lista(id_alien,ALIEN);
            cria_alien(ALIEN,alien_x,alien_y,id_alien,tipo_alien3);
            alien_y += 7;
        }
        id_alien++;
	}
}

void print_alien(t_lista *a, int count, int *mudar_perna) {
    int x, y;
	int i;

	if (!lista_vazia(a)) {
		t_nodo *no; 
		no = a->ini;

		if (count % 16 == 0)
			*mudar_perna = !(*mudar_perna);

		const char *ALIEN1_1[3] = { "(+.+)",
							  	    "\\nnn/",
							        " /X\\ " };
		
		const char *ALIEN1_2[3] = { "(x-x)",
							  	    "-nnn-",
							        " { } " };					      
		
		const char *ALIEN2_1[3] = { ">v.v<",
								    "\\[X]/",
								    " / \\" };

		const char *ALIEN2_2[3] = { "<o.o>",
								    "/[X]\\",
								    " ( ) " };

		const char *ALIEN3_1[3] = { " >.< ",
								    "dooob",
								    " [ ] "};
		
		const char *ALIEN3_2[3] = { " x.x ",
								    "-ooo-",
								    " ] [ "};

		const char *EXPLOSION[3] = {"\\ | /",
									"-   -",
									"/ | \\"};

		while (no != NULL) {
			
			x = no->coord.x;
			y = no->coord.y;
			
			/* verifica o estado atual do alien antes de imprimir */
			wattron(stdscr, COLOR_PAIR(5));
			if (no->status == morrendo) {
				for (i = 0; i < 3; i++) {
					move(x+i,y);
					printw("%s",EXPLOSION[i]);
				}
				if (count % 8 == 0)
					no->status = morto;
			}
			else if (no->status == vivo) {
				wattron(stdscr, COLOR_PAIR(1));
				if (no->tipo == tipo_alien1) {
					if (*mudar_perna) {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN1_1[i]);
						}
					}
					else {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN1_2[i]);
						}
					}
				}
				else if (no->tipo == tipo_alien2) {
					if (*mudar_perna) {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN2_1[i]);
						}
					}
					else {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN2_2[i]);
						}
					}
				}
				else if (no->tipo == tipo_alien3) {
					if (*mudar_perna) {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN3_1[i]);
						}
					}
					else {
						for (i = 0; i < 3; i++) {
							move(x+i,y);
							printw("%s",ALIEN3_2[i]);
						}
					}
				}
			}
			/* quando a explosao acaba, o alien eh removido */
			if (no->status == morto) {
				t_nodo *aux;
				aux = no->prox;
				remove_item_lista(a,no->ID);
				no = aux;
			}
			else 
				no = no->prox;
		}
	}
}

/* cria a barreira, sendo que cada pedaco da barreira eh um nodo */
void cria_barreira (int id_barreira, int ini_x, int ini_y, t_lista *barreira) {
	t_nodo *p;
	p = barreira->ini;
	
	while (p->ID != id_barreira) 
		p = p->prox;
	
	p->tipo = tipo_barreira;
	p->coord.x = ini_x;
	p->coord.y = ini_y;
	p->tam_x = 1;
	p->tam_y = 1;
}

/* organiza a barreira de forma que existam 4 delas */
void inicia_barreira(t_lista *barreira) {
	int i, j, k;
	int ini_x, ini_y = 15;
	int count = 0;
	int x_atual, y_atual;

	for (k = 0; k < 4; k++) {
		ini_x = 30;
		for (i = 0; i < 3; i++) {
			x_atual = ini_x + i;
			for (j = 0; j < 7; j++) {
				y_atual = ini_y + j;
				insere_fim_lista(count,barreira);
				cria_barreira(count,x_atual,y_atual,barreira);
				count++;
			}
		}
		ini_y += 20;
	}
}

void print_barreira(t_lista *barreira) {
	int x, y;

	t_nodo *p;
	p = barreira->ini;
	
	wattron(stdscr, COLOR_PAIR(4));
	while (p != NULL) {
		y = p->coord.y;
		x = p->coord.x;
		move(x,y);
		printw("o");
		p = p->prox;
	}
}

/* armazena as informacoes iniciais do jogador */
void inicia_jogador (t_lista *player) {
	int ini_x = 34;
	int ini_y = 2;

	insere_fim_lista(1,player);
	t_nodo *p;
	p = player->ini;

	p->coord.x = ini_x;
	p->coord.y = ini_y;
	p->max.x = ini_x + 1;
	p->max.y = ini_y + 4;
	p->tam_x = 2;
	p->tam_y = 5;
	p->direcao = 1;
	p->status = vivo;
	p->tipo = tipo_jogador;
}

/* controla os movimentos do jogador */
void controla_coord_jogador (t_lista *player, int count_y) {
	player->ini->coord.y += count_y;
	player->ini->max.y += count_y;
}

void print_jogador (t_lista *player) {
	int x, y;
	int i;
	const char *player_body1[2] = {" /^\\ ",
								   "MmMmM"};
	const char *player_body2[2] = {" /^\\ ",
								   "mMmMm"};
    
    t_nodo *p;
    p = player->ini;
    if (p->status == morto)
    	return;
    
    y = p->coord.y;
	x = p->coord.x;
    
	wattron(stdscr, COLOR_PAIR(7));
    for (i = 0; i < 2; i++) {
		move(x+i,y);
		if (y % 2 == 0)
			printw("%s",player_body1[i]);
		else
			printw("%s",player_body2[i]);
		
	}
}

void controla_bombas (t_lista *bombas, int count, int type) {
	if (lista_vazia(bombas))
		return;

	char *shot;
    int x, y, parametro;
	t_nodo *aux;
	aux = bombas->ini;
	while (aux != NULL) {
		if (aux->tipo == tipo_bomba) {
			wattron(stdscr, COLOR_PAIR(5));
			parametro = 36; /* para as bombas dos aliens */
			shot = "$";
		}
		else if (aux->tipo == tipo_tiro) {
			parametro = 1; /* para os tiros do player */
			wattron(stdscr, COLOR_PAIR(4));
			shot = "|";
		}
		
		/* confere se a bomba ou o tiro chegaram no seu limite da borda
		horizontal */
		if (aux->coord.x == parametro)
			remove_item_lista(bombas,aux->ID);
		else {
			x = aux->coord.x;
			y = aux->coord.y;
			move(x,y);
			printw("%s",shot);
			
			if (count % 4 == 0) {
				if (type == tipo_tiro)
					aux->coord.x--;
				else if (type == tipo_bomba)
					aux->coord.x++;
			}
		}
		aux = aux->prox;
	}
}

/* armazena as informacoes da bomba ou do tiro */
void cria_bomba (t_lista *b, int *id_tiro, int bomba_x, int bomba_y, int tipo_da_bomba) {
	t_nodo *p;
	p = b->ini;

	while (p->ID != *id_tiro)
		p = p->prox;

	p->coord.x = bomba_x;
	p->coord.y = bomba_y;
	p->tipo = tipo_da_bomba;
	p->status = 1;
	p->direcao = 1;
	
	p->tam_x = 1;
    p->tam_y = 1;
    p->max.x = bomba_x;
    p->max.y = bomba_y;
	(*id_tiro)++;
}

/* sorteia um alien aleatoriamente e usa o cont do tempo para decidir se atira ou nao */
void atira_aleatorio (t_lista *bombas, t_lista *alien, int *id_bomba, int count) {

	int shot;
	shot = rand() % (alien->tamanho);

	if (count % 32 == 0) {
		t_nodo *aux_alien;
		int id_objeto = *id_bomba;

		aux_alien = alien->ini;

		int count_alien;
		for (count_alien = 0; count_alien < shot; count_alien++)
			aux_alien = aux_alien->prox;

		int bomb_x = (aux_alien->coord.x)+3;
		int bomb_y = (aux_alien->coord.y)+2;

		insere_fim_lista(id_objeto,bombas);
		cria_bomba(bombas,id_bomba,bomb_x,bomb_y,tipo_bomba);
	}

	if (!lista_vazia(bombas))
		controla_bombas(bombas,count,tipo_bomba);
}


/* nessa funcao eh controlada a tecla que o usuario digita.
se for digitado q ele sai do jogo e se for digitada a tecla
esquerda ou a tecla direita a funcao controla o movimento
do jogador */
void controle (t_lista *player, t_lista *b, int *id_tiro, int count) {
	nodelay(stdscr, TRUE);

	int move = 0;
	t_nodo *p;
	p = player->ini;

	int y = p->coord.y;
	int bomb_x = (p->coord.x)-1;
	int bomb_y = (p->coord.y)+2;
	int id_objeto = *id_tiro;
	
	switch (getch()) {
		case ' ':
		/* eh colocado um limite de 10 tiros na tela */
		if (b->tamanho < 10) {
			insere_fim_lista(id_objeto,b);
			cria_bomba(b,id_tiro,bomb_x,bomb_y,tipo_tiro);
		}
		break;
		case KEY_LEFT:
			if (y > 2)
				move = -1;
		break;
		case KEY_RIGHT:
			if (y+6 < 100)
				move = 1;
		break;
		case 'q':
		endwin();
		exit(1);
		break;
	}
	controla_bombas(b,count,tipo_tiro);
	controla_coord_jogador(player,move);
	print_jogador(player);
} 

/* controla o movimento dos aliens, decidindo se ira descer ou nao e
se move para a esquerda ou direita */
void move_aliens(t_lista *a, int *rapido, int *mudar_perna) {
	if (lista_vazia(a))
		return;

	int borda_esquerda = 0, borda_direita = 0;
	*rapido = 0;
	
	t_nodo *no;
	no = a->ini;
	while (no->prox != NULL && no->coord.y != 2 && no->max.y != 98)
		no = no->prox;

	if (no->coord.y == 2) {
		borda_esquerda = 1;
	}
	else if (no->max.y == 98) {
		borda_direita = 1;
	}

	no = a->ini;
	while (no != NULL) {
		/* se chegar na borda ele retorna na variavel rapido que
		o jogo deve ficar mais acelerado */
		if (borda_direita) {
			no->coord.x += 1;
			no->max.x += 1;
			no->direcao = esquerda;
			*rapido = 1;
		}
		else if (borda_esquerda) {
			no->coord.x += 1;
			no->max.x += 1;
			no->direcao = direita;
			*rapido = 1;
		}

		if (no->direcao == esquerda) {
	    	--(no->coord.y);
	    	--(no->max.y);
		}
	    else if (no->direcao == direita) {
	        ++(no->coord.y);
	        ++(no->max.y);
	    }
		no = no->prox;
	}
} 

/* armazena as informacoes da nave mae */
void inicia_nave (t_lista *ship) {
	insere_fim_lista(1,ship);
	t_nodo *p;
	p = ship->ini;
	int ini_x = 2, ini_y = 1;

	p->coord.x = ini_x;
	p->coord.y = ini_y;
	p->max.x = ini_x + 2;
	p->max.y = ini_y + 8;
	p->tam_x = 3;
	p->tam_y = 9;
	p->status = vivo;
	p->tipo = tipo_nave;
	p->direcao = direita;
}

void print_nave (t_lista *ship) {
	if (lista_vazia(ship))
		return;
	
	int x, y;
	int i;
	t_nodo *p;
	p = ship->ini;
	
	const char *ship_body[3] = {" /MMMMM\\ ",
								"AMoMoMoMA",
								" \\/'-'\\/ "};
	wattron(stdscr, COLOR_PAIR(3));
	for (i = 0; i < p->tam_x; i++) {
		if (p->status == vivo) {
			y = p->coord.y;
			x = p->coord.x;
			move(x+i,y);
			printw("%s",ship_body[i]);
		}
	}
}

/* controla o movimento da nave mae */
void move_nave (t_lista *ship, int count) {
	if (lista_vazia(ship))
		return;
	
	t_nodo *p;
	p = ship->ini;
	
	if (p->max.y < 100) {
		++p->coord.y;
		++p->max.y;
	}
	else {
		/* decide se a nave mae aparece ou nao */
		p->status = morrendo;
		if (count % 500 == 0) {
			p->status = vivo;
			p->coord.y = 1;
			p->max.y = 1 + p->tam_y;
		}
	}
}

/* trata a remocao para que nao seja acessado, posteriormente,
um ponteiro ja removido */
void trata_remocao (t_lista *lista, t_nodo **elemento) {
	t_nodo *aux_elemento;
	aux_elemento = (*elemento)->prox;
	remove_item_lista(lista,(*elemento)->ID);
	*elemento = aux_elemento;
}

/* checa se alguma das coordenadas de dois objetos sao iguais,
detectando assim uma colisao */
int checa_coord (t_nodo *elemento, t_nodo *bombas) {
	int i, j, k, l;

	for (i = 0; i < elemento->tam_x; i++) {
		for (j = 0; j < elemento->tam_y; j++) {
			for (k = 0; k < bombas->tam_x; k++) {
				for (l = 0; l < bombas->tam_y; l++) {
					if ((elemento->coord.x)+i == bombas->coord.x && (elemento->coord.y)+j == bombas->coord.y) {
					return 1;
					}
				}
			}
		}
	}
	return 0;
}

void checa_colisao (t_lista *bombas, t_lista *elemento, int *score) {
	if (lista_vazia(bombas) || lista_vazia(elemento))
		return;
	
	t_nodo *p_bombas;
	p_bombas = bombas->ini;

	while (p_bombas != NULL) {
		t_nodo *aux_elemento;
		aux_elemento = elemento->ini;
		int achou_elemento = 0;

		while (aux_elemento != NULL && !achou_elemento) {
			if (checa_coord(aux_elemento,p_bombas)) {
				
				if (aux_elemento->tipo != tipo_alien1 && aux_elemento->tipo != tipo_alien2 && aux_elemento->tipo != tipo_alien3 ) {
					if (aux_elemento->tipo == tipo_nave)
						(*score) += 250;

					trata_remocao(elemento,&aux_elemento);
				}
				else {
					if (p_bombas->tipo == tipo_tiro) { 
						aux_elemento->status = morrendo;
						(*score) += 100;
					}
				}
				trata_remocao(bombas,&p_bombas);
				achou_elemento = 1;
			}
			if (!achou_elemento)
				aux_elemento = aux_elemento->prox;
		}
		
		if (!achou_elemento)
			p_bombas = p_bombas->prox;
	}
}

/* checa se o jogador morreu */
int checa_jogador (t_lista *player) {
	return (!lista_vazia(player));
}

/* checa se os aliens chegaram na ultima linha do jogo */
int checa_aliens (t_lista *alien) {
	t_nodo *p_alien;
	p_alien = alien->ini;
	int i;

	while (p_alien != NULL) {
		for (i = 0; i < p_alien->tam_x; i++) {
			if (p_alien->coord.x + i == 36) 
				return 0;
		}
		p_alien = p_alien->prox;
	}
	return 1;
}

void imprime_vitoria (int score) {
	clear();
	mvprintw(16,45,"VOCE GANHOU");
    mvprintw(17,45,"Seu score: %d",score);
	mvprintw(18,45,"Espere alguns instantes");
    refresh();
    usleep(7000000);
	endwin();
	exit(1);
}

/* reorganiza o jogo para a proxima rodada */
void reinicia_jogo (t_lista *alien, t_lista *bombas, t_lista *tiros, t_lista *barreira, t_lista *nave) {
	destroi_lista(bombas);
	destroi_lista(tiros);
	destroi_lista(barreira);
	destroi_lista(nave);
	inicia_aliens(alien);
	inicia_barreira(barreira);
	inicia_nave(nave);
}

/* deixa o jogo mais rapido para a proxima rodada */
void controla_velocidade (int *velocidade, int *velocidade_ini) {
	*velocidade = *velocidade_ini - 4;
	*velocidade_ini -= 4;
}

void remove_todos (t_lista *alien, t_lista *bombas, t_lista *tiros, t_lista *barreira, t_lista *nave, t_lista *player) {
	destroi_lista(bombas);
	destroi_lista(tiros);
	destroi_lista(barreira);
	destroi_lista(nave);
	destroi_lista(alien);
	destroi_lista(player);
}