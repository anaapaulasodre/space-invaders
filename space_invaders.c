#include<stdio.h>
#include<stdlib.h>
#include<ncurses.h>
#include<unistd.h>
#include<string.h>
#include "funcoes_jogo.h"

int main () {
        initscr();
        cbreak();
        noecho();
        curs_set(FALSE);
        
        int largura = 101, altura = 37;
        int nlin, ncol;

        wattron(stdscr,A_BOLD);
        
        keypad(stdscr, true);

        /* inicializa as cores */
        start_color();
        init_pair(1, COLOR_WHITE, COLOR_BLACK);
        init_pair(2, COLOR_GREEN, COLOR_BLACK);
        init_pair(3, COLOR_RED, COLOR_BLACK);
        init_pair(4, COLOR_BLUE, COLOR_BLACK);
        init_pair(5, COLOR_YELLOW, COLOR_BLACK);
        init_pair(6, COLOR_CYAN, COLOR_BLACK);
        init_pair(7, COLOR_MAGENTA, COLOR_BLACK);

        wattron(stdscr, COLOR_PAIR(1));

        getmaxyx(stdscr, nlin, ncol);
        if (nlin < altura && ncol < largura) {
                endwin();
                printf("O terminal deve ter no mínimo 38 linhas e 100 colunas\n");
                exit(1);
        }
        
        t_lista player;
        t_lista ALIEN;        
        t_lista nave;
        t_lista tiros;
        t_lista bombas;
        t_lista barreira;

        /* inicializa as listas do jogo */
        inicializa_lista(&player);
        inicia_jogador(&player);

        inicializa_lista(&barreira);
        inicia_barreira(&barreira);

        inicializa_lista(&ALIEN);
        inicia_aliens(&ALIEN);

        inicializa_lista(&tiros);
        inicializa_lista(&bombas);

        inicializa_lista(&nave);
        inicia_nave(&nave);

        int count = 0, velocidade = 16, mudar_perna = 0, id_tiro = 0;
        int id_bomba = 0, rapido = 0, velocidade_ini = velocidade;
        int rodadas = 0, reinicia = 0, score = 0;;
        
        /* o count ira auxiliar para controlar se o objeto deve se 
        mover ou nao */
        while (rodadas < 3) {
                if (reinicia) {
                        reinicia_jogo(&ALIEN,&bombas,&tiros,&barreira,&nave);
                        controla_velocidade(&velocidade,&velocidade_ini);
                        count = 0;
                        reinicia = 0;
                }

                if (checa_jogador(&player) && checa_aliens(&ALIEN)) {
                        erase();
                        print_placar(score,rodadas);
                        print_barreira(&barreira);
                        
                        if (count % 2 == 0)
                        	move_nave(&nave,count);
                        print_nave(&nave);

                        atira_aleatorio (&bombas,&ALIEN,&id_bomba,count);
                        controle(&player,&tiros,&id_tiro,count);
                        
                        if (rapido && velocidade > 2)
                                velocidade -= 2;
                        rapido = 0;

                        if (count % velocidade == 0)
                                move_aliens(&ALIEN,&rapido,&mudar_perna); 
                        print_alien(&ALIEN,count,&mudar_perna);

                        checa_colisao(&bombas,&barreira,&score);
                        checa_colisao(&tiros,&barreira,&score);
                        checa_colisao(&tiros,&ALIEN,&score);
                        checa_colisao(&barreira,&ALIEN,&score);
                        checa_colisao(&player,&ALIEN,&score);
                        checa_colisao(&tiros,&nave,&score);
                        checa_colisao(&bombas,&player,&score);

                        if (lista_vazia(&ALIEN)) {
                                rodadas++;
                                reinicia = 1;
                        }
                        desenha_borda();
                }
                else {
                        remove_todos(&ALIEN,&bombas,&tiros,&barreira,&nave,&player);
                        endwin();
                        printf("Tente novamente :)\n");
                        exit(1);
                }
                count++;
                
                refresh();
                usleep(20000);
        }
        remove_todos(&ALIEN,&bombas,&tiros,&barreira,&nave,&player);
        imprime_vitoria(score);
        return 0;
}