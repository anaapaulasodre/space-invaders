#include <stdio.h>
#include <stdlib.h>
#define vivo 1
#define morrendo 2
#define morto 3
#define direita 4
#define esquerda -4
#define tipo_barreira 5
#define tipo_bomba 6
#define tipo_tiro 7
#define tipo_jogador 8
#define tipo_nave 9
#define tipo_alien1 10
#define tipo_alien2 11
#define tipo_alien3 12

struct T_COORD {
	int x;
	int y;
};
typedef struct T_COORD T_COORD;

struct t_nodo {
    int direcao;
    int status;
    int tipo;
    int ID;
    int tam_x;
    int tam_y;
    struct T_COORD coord;
    struct T_COORD max;
    struct t_nodo *prox;
};
typedef struct t_nodo t_nodo;

struct t_lista {
    t_nodo *ini;
    int tamanho;
};
typedef struct t_lista t_lista;

void inicializa_lista(t_lista *lista);

int lista_vazia(t_lista *lista);

void destroi_lista(t_lista *lista);

void insere_inicio_lista(int x, t_lista *lista);

void insere_fim_lista(int id_objeto, t_lista *lista);

void remove_primeiro_lista(t_lista *lista);

void remove_ultimo_lista(t_lista *lista);

void remove_item_lista  (t_lista *lista, int id_objeto);