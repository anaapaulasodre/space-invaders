#include "lib_lista_jogo.h"

void print_placar (int score, int rodadas);

void desenha_borda ();

void cria_alien (t_lista *alien, int alien_x, int alien_y, int id_alien, int tipo_alien);

void inicia_aliens (t_lista *ALIEN);

void print_alien(t_lista *a, int count, int *mudar_perna);

void cria_barreia (int id_barreira, int ini_x, int ini_y, t_lista *barreira);

void inicia_barreira(t_lista *barreira);

void print_barreira(t_lista *barreira);

void inicia_jogador (t_lista *player);

void controla_coord_jogador (t_lista *player, int count_y);

void print_jogador (t_lista *player);

void controla_bombas(t_lista *bombas, int count, int type);

void cria_bomba (t_lista *b, int *id_tiro, int bomba_x, int bomba_y, int tipo_da_bomba);

void atira_aleatorio (t_lista *bombas, t_lista *alien, int *id_bomba, int count);

void controle (t_lista *player, t_lista *b, int *id_tiro, int count);

void move_aliens(t_lista *a, int *rapido, int *mudar_perna);

void inicia_nave (t_lista *ship);

void print_nave(t_lista *ship);

void move_nave (t_lista *ship, int count);

void trata_remocao (t_lista *lista, t_nodo **elemento);

int checa_coord (t_nodo *elemento, t_nodo *bombas);

void checa_colisao (t_lista *bombas, t_lista *elemento, int *score);

int checa_jogador (t_lista *player);

int checa_aliens (t_lista *alien);

void imprime_vitoria (int score);

void reinicia_jogo (t_lista *alien, t_lista *bombas, t_lista *tiros, t_lista *barreira, t_lista *nave);

void controla_velocidade (int *velocidade, int *velocidade_ini);

void remove_todos (t_lista *alien, t_lista *bombas, t_lista *tiros, t_lista *barreira, t_lista *nave, t_lista *player);