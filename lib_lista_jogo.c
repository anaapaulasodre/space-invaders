#include "lib_lista_jogo.h"

void inicializa_lista(t_lista *lista) {
	lista->ini = NULL;
	lista->tamanho = 0;
}

int lista_vazia(t_lista *lista) {
	return (lista->tamanho == 0); 
}

void destroi_lista(t_lista *lista) {
	if (lista_vazia(lista)) 
		return;
	
	t_nodo *no, *aux;
	no = lista->ini;
	
	/* a lista será destruída a partir do começo */
	while (no != NULL) {
		aux = no;
		no = no->prox;
		free(aux); 
		lista->tamanho--;
	}
	lista->ini = NULL;
	free(no); 
}

void insere_inicio_lista(int x, t_lista *lista) {
	t_nodo *novo;
       	novo = (t_nodo *) malloc(sizeof(t_nodo));
	if (novo == NULL)
		return;
	novo->ID = x;
	
	/* se a lista estiver vazia, o primeiro elemento da lista será o elemento x */
	if (lista_vazia(lista))
		novo->prox = NULL;
	else {
		novo->prox = lista->ini;
	}
	lista->ini = novo;
	lista->tamanho++;
}

void insere_fim_lista(int id_objeto, t_lista *lista) {
	t_nodo *novo;
    novo = (t_nodo *) malloc(sizeof(t_nodo));
	
	if (novo == NULL)
		return;
	
	novo->ID = id_objeto;
	novo->prox = NULL;
	
	if (lista_vazia(lista))
		lista->ini = novo;
	else {
		t_nodo *aux; 
		aux = lista->ini;
		while (aux->prox != NULL)
			aux = aux->prox;
		aux->prox = novo;
	}	
	
	lista->tamanho++;
}

void remove_primeiro_lista(t_lista *lista) {
	if (lista_vazia(lista)) 
		return;

	t_nodo *p;
	p  = lista->ini;
	
	if (p->prox == NULL) {
		lista->ini = NULL;
		free(p);
		lista->tamanho--;
	}
	
	lista->ini = p->prox;
	free(p);  
	lista->tamanho--;
}

void remove_ultimo_lista(t_lista *lista) {
	if (lista_vazia(lista))
		return;
	
	t_nodo *p; 
	p = lista->ini;
	
	if (p->prox == NULL) {
		free (lista->ini);
		lista->ini = NULL;
		lista->tamanho--;
	}
	
	while (p->prox->prox != NULL)
		p = p->prox;
	
	free(p->prox); 
	p->prox = NULL;
	
	lista->tamanho--;
}


void remove_item_lista(t_lista *lista, int id_objeto) {
	if (lista_vazia(lista))
		return;

	t_nodo *p;

	p = lista->ini;
	if (p->ID == id_objeto) {
		if (p->prox == NULL) {
			free(p);
			lista->ini = NULL;
			lista->tamanho--;
			return;
		}
		
		free(p);
		lista->ini = p->prox;
		lista->tamanho--;
		return;
	}

	while (p->prox->prox != NULL && p->prox->ID != id_objeto)
		p = p->prox;

	if (p->prox->prox == NULL && p->prox->ID == id_objeto) {
		free(p->prox);
		p->prox = NULL;
		lista->tamanho--;
		return;	
	}
	
	if (p->prox->prox != NULL && p->prox->ID == id_objeto) {
		t_nodo *aux;

		aux = p->prox->prox;
		p->prox->prox = NULL;
		free(p->prox);
		p->prox = aux;
		lista->tamanho--;
		
	}
}
